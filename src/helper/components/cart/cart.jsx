import React from 'react'
import PropTypes from 'prop-types'
// helper
import { toPersian } from '../../functions/utils'
// style
import './cart.css'

const CartItem = ({ foodName, count, price, onIncrease, onDecrease }) => (
  <div>
    <div className='cart-item'>
      <p>{foodName}</p>
      <div className='change-food-counter'>
        <div className='cart-counter'>
          <span className='flaticon-plus pointer' onClick={onIncrease}></span>
          <p className='no-select'>{toPersian(count)}</p>
          <span className='flaticon-minus pointer' onClick={onDecrease}></span>
        </div>
        <p className='food-price'>{toPersian(price)} تومان</p>
      </div>
    </div>
    <div className='cart-item-line'></div>
  </div>
)

CartItem.propTypes = {
  foodName: PropTypes.string.isRequired,
  count: PropTypes.number.isRequired,
  price: PropTypes.number.isRequired,
  onIncrease: PropTypes.func.isRequired,
  onDecrease: PropTypes.func.isRequired,
}

const Cart = ({ foods, totalPrice, onIncrease, onDecrease, onDone }) => (
  <div className='cart-container'>
    <div>
      <p className='cart-header'>سبد خرید</p>
      <div className='title-underline'></div>
    </div>
    <div className='cart-body'>
      {foods.length ? (
        foods.map(({ foodId, foodName, restaurantId, count, price }) => (
          <CartItem
            key={foodId}
            foodName={foodName}
            count={count}
            price={price}
            onIncrease={onIncrease({ restaurantId, foodId, foodName, price })}
            onDecrease={onDecrease(foodId)}
          />
        ))
      ) : (
        <p className='cart-empty center no-select'>سبد شما خالی است</p>
      )}
    </div>
    <div className='total-amount-box'>
      <p>جمع کل:</p>
      <p className='total-amount'>{toPersian(totalPrice)} تومان</p>
      <button
        className='btn submit-blue'
        onClick={onDone}
        disabled={!foods.length}
      >
        تأیید نهایی
      </button>
    </div>
  </div>
)

Cart.propTypes = {
  foods: PropTypes.array.isRequired,
  totalPrice: PropTypes.number.isRequired,
  onIncrease: PropTypes.func.isRequired,
  onDecrease: PropTypes.func.isRequired,
  onDone: PropTypes.func.isRequired,
}

export default Cart
