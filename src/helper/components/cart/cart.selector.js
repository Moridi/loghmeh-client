// modules
import * as R from 'ramda'
import { createSelector } from 'reselect'

const getUserCart = R.pathOr([], ['main', 'user', 'cart'])

export const totalPriceSelector = createSelector(
  [getUserCart],
  R.reduce((sum, { price, count }) => sum + price * count, 0),
)
