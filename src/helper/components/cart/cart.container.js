// modules
import { connect } from 'react-redux'
// components
import Cart from './cart'
// actions
import { dispatchRemoveFromCart } from '../../../logic/user/user.action'
// selectors
import { totalPriceSelector } from './cart.selector'
// requests
import { finalizeOrder } from '../../../logic/user/user.request'
// helpers
import { addToCart } from '../../../logic/user/user.helper'

const mapStateToProps = state => ({
  foods: state.main.user.cart || [],
  totalPrice: totalPriceSelector(state),
})

const mapDispatchToProps = () => ({
  onIncrease: food => () => addToCart(food, 1),
  onDecrease: foodId => () => dispatchRemoveFromCart(foodId, 1),
  onDone: finalizeOrder,
})

export default connect(mapStateToProps, mapDispatchToProps)(Cart)
