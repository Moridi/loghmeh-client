import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'
// components
import { Dialog } from '@material-ui/core'
// helpers
import { cns } from '../../../helper/functions/utils'

// style
const useStyles = makeStyles(() => ({
  paper: {
    maxWidth: 'none',
    borderRadius: 10,

    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'inherit',

    boxSizing: 'border-box',
    padding: 14,
  },
  backdrop: {
    backgroundColor: '#000000b3',
  },
}))

const MyDialog = ({ isOpen, className, children, onClose }) => {
  const classes = useStyles()

  return (
    <Dialog
      open={isOpen}
      PaperProps={{
        className: cns(classes.paper, className),
      }}
      BackdropProps={{
        className: classes.backdrop,
      }}
      onClose={onClose}
    >
      {children}
    </Dialog>
  )
}

MyDialog.propTypes = {
  isOpen: PropTypes.bool,
  className: PropTypes.string,
  children: PropTypes.array,
  onClose: PropTypes.func,
}

MyDialog.defaultProps = {
  isOpen: false,
  className: '',
  children: [],
  onClose: Function.prototype,
}

export default MyDialog
