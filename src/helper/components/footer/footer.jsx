import React from 'react'
// style
import './footer.css'

export default () => (
  <div className='footer'>
    <p>Ⓒ تمامی حقوق برنامه متعلق به لقمه است.</p>
  </div>
)
