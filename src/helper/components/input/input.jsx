import React from 'react'
import PropTypes from 'prop-types'

const isNonEnglish = string => string.charCodeAt(0) > 127

const getDirection = value => (!value || isNonEnglish(value) ? 'rtl' : 'ltr')

const getTextAlign = value => (!value || isNonEnglish(value) ? 'right' : 'left')

const Input = ({ value, placeholder, type, className, onChange }) => (
  <input
    value={value}
    type={type}
    dir={getDirection(value)}
    style={{ textAlign: getTextAlign(value) }}
    className={`form-control ${className}`}
    placeholder={placeholder}
    onChange={e => onChange(e.target.value)}
  />
)

Input.propTypes = {
  value: PropTypes.string,
  placeholder: PropTypes.string,
  type: PropTypes.string,
  className: PropTypes.string,
  onChange: PropTypes.func,
}

Input.defaultProps = {
  value: '',
  placeholder: '',
  type: 'text',
  className: '',
  onChange: () => {},
}

export default Input
