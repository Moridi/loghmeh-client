// modules
import React from 'react'
import PropTypes from 'prop-types'
import CloseIcon from '@material-ui/icons/Close'
// style
import './closeButton.css'

const CloseButton = ({ onClick }) => (
  <button className='btn close-btn center' onClick={onClick}>
    <CloseIcon classes={{ root: 'close-icon' }} />
  </button>
)

CloseButton.propTypes = {
  onClick: PropTypes.func,
}

CloseButton.defaultProps = {
  onClick: Function.prototype,
}

export default CloseButton
