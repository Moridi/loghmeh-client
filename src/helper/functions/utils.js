export const ab = className => test => (test ? className : undefined)

export const cns = (...args) => {
  let index = 0
  let classNames = ''
  while (index < args.length) {
    classNames += !args[index] ? '' : `${args[index]} `
    index += 1
  }
  return classNames.trim()
}

export const toPersian = text => {
  if (text == null) return text
  if (typeof text === 'number') return text.toLocaleString('fa-IR')
  if (typeof text === 'string')
    return text.replace(/[0-9]/g, num => parseInt(num).toLocaleString('fa-IR'))
}

const format = number => (number < 10 ? `0${number}` : number)

export const formattedSeconds = time =>
  `${format(Math.floor(time / 60))}:${format(time % 60)}`
