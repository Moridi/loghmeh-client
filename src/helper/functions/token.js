// actions
import { dispatchSetUserToken } from '../../logic/user/user.action'

const storage = window.localStorage

export const setToken = token => {
  storage.setItem('token', token)
  dispatchSetUserToken(token)
}

export const getTokenFromStorage = () => storage.getItem('token')

export const removeToken = () => {
  storage.removeItem('token')
  dispatchSetUserToken('')
}
