import viewReducers from '../app/app.reducer'
import mainReducers from '../logic/main/main.reducer'

export default {
  view: viewReducers,
  main: mainReducers,
}
