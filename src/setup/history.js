import { createHistory, createMemorySource } from '@reach/router'

const source = createMemorySource('/loading')
export const history = createHistory(source)

export const { navigate } = history
