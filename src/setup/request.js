// modules
import { create } from 'axios'
import { dispatchSetSnackbar } from '../app/components/snackbar/snackbar.actions'
import { dispatchSetIsLoading } from '../app/components/loading/loading.action'
// helper
import errCodes from './errCodes'
import { userTokenView } from '../logic/user/user.reducer'
import { logoutUser } from '../logic/user/user.request'

// const server = 'http://localhost:8080'
const server = 'http://185.166.105.6:30085'

const { get, post, put } = create({
  baseURL: server,
})

const catchErrors = handler => (...args) =>
  handler(...args).catch(err => {
    const { data, status } = err.response || {}
    if (status === 400 || status === 401 || status === 403 || status === 404)
      dispatchSetSnackbar(errCodes[data], 'error')
    if (status === 401) logoutUser()

    throw err
  })

export const sleep = time => new Promise(res => setTimeout(res, time))

export const withLoading = handler => (...args) => {
  dispatchSetIsLoading(true)
  return sleep(1000)
    .then(() => handler(...args))
    .finally(() => dispatchSetIsLoading(false))
}

export const sendGet = catchErrors((url, query) =>
  get(url, {
    params: query,
    headers: {
      authorization: userTokenView(),
    },
  }),
)

export const sendPost = catchErrors((url, data, params) =>
  post(url, data, {
    params,
    headers: {
      authorization: userTokenView(),
    },
  }),
)

export const sendPut = catchErrors((url, body) =>
  put(url, null, {
    params: body,
    headers: {
      authorization: userTokenView(),
    },
  }),
)
