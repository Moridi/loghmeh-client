export default {
  DISTANT_RESTAURANT: 'رستوران در محدوده‌ی شما نیست',
  DUPLICATE_FOOD: 'این غذا قبلا ثبت شده است',
  DUPLICATE_RESTAURANT: 'این رستوران قبلا ثبت شده است',
  EMPTY_CART: 'سبد خرید شما خالی است',
  FOOD_NOT_FOUND: 'غذای موردنظر یافت نشد',
  IMPROPER_RESTAURANT: 'رستوران‌های متفاوت نمی‌توانند در سبد شما باشند',
  NO_CREDIT: 'موجودی شما کافی نیست',
  NO_SUCH_ORDER: 'سفارش موردنظر یافت نشد',
  PARTY_FOOD_EXPIRED: 'مهلت جشن غذا به اتمام رسیده است',
  PARTY_FOOD_SOLD_OUT: 'تقاضای شما بیش از موجودی غذا در جشن غذا است',
  RESTAURANT_NOT_FOUND: 'رستوران موردنظر یافت نشد',
  INVALID_USER_DATA: 'اطلاعات کاربری وارد شده اشتباه است',
  DUPLICATE_USER: 'کاربر دیگری با این ایمیل وجود دارد',
  INVALID_TOKEN: 'کاربر غیرمجاز',
}
