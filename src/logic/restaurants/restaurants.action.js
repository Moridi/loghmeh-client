// modules
import { createAction } from 'redux-actions'
// setup
import { dispatch } from '../../setup/redux'

export const ADD_RESTAURANTS = 'ADD_RESTAURANTS'
export const dispatchAddRestaurants = (...args) =>
  dispatch(createAction(ADD_RESTAURANTS)(...args))
