import { sendGet, withLoading } from '../../setup/request'
// actions
import { dispatchAddRestaurants } from './restaurants.action'
import {
  dispatchAddSearchResults,
  dispatchSetShowMoreVisibility,
} from '../../app/components/home/home.action'
// setup
import { getState } from '../../setup/redux'

const BUCKET_SIZE = 15

const restaurantsView = () => getState().main.restaurants || []
const searchResultsView = () => getState().view.home.searchResults || []

export const fetchRestaurants = () =>
  sendGet('/restaurant', {
    startAt: restaurantsView().length,
  }).then(({ data }) => {
    dispatchAddRestaurants(data)
    dispatchSetShowMoreVisibility(data.length >= BUCKET_SIZE)
  })

export const getRestaurantById = withLoading(id =>
  sendGet(`/restaurant/${id}`).then(({ data }) => data),
)

export const searchRestaurants = withLoading((restaurantName, foodName) =>
  sendGet('/search', {
    restaurantName,
    foodName,
    startAt: searchResultsView().length,
  }).then(({ data }) => {
    dispatchAddSearchResults(data)
    dispatchSetShowMoreVisibility(data.length >= BUCKET_SIZE)
  }),
)
