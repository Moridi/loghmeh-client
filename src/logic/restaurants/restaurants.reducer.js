import { ADD_RESTAURANTS } from './restaurants.action'

const initialState = []

const reducers = {
  [ADD_RESTAURANTS]: (state, restaurants) => [...state, ...restaurants],
}

export default (state = initialState, { type, payload }) =>
  reducers[type] ? reducers[type](state, payload) : state
