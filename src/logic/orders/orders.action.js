// modules
import { createAction } from 'redux-actions'
// setup
import { dispatch } from '../../setup/redux'

export const SET_ORDERS = 'SET_ORDERS'
export const dispatchSetOrders = (...args) =>
  dispatch(createAction(SET_ORDERS)(...args))

export const ADD_ORDER = 'ADD_ORDER'
export const dispatchAddOrder = (...args) =>
  dispatch(createAction(ADD_ORDER)(...args))
