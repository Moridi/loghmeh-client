import { SET_ORDERS } from './orders.action'

const initialState = []

const reducers = {
  [SET_ORDERS]: (state, orders) => orders,
}

export default (state = initialState, { type, payload }) =>
  reducers[type] ? reducers[type](state, payload) : state
