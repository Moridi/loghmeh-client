import { sendGet } from '../../setup/request'
// actions
import { dispatchSetOrders } from './orders.action'

export const fetchUserOrders = () =>
  sendGet('/order').then(({ data }) => dispatchSetOrders(data))
