import { sendGet } from '../../setup/request'
// actions
import {
  dispatchSetFoodParty,
  dispatchDecrementRemainingTime,
} from './foodparty.action'

let interval
let timer

export const fetchFoodParty = () =>
  sendGet('/foodParty').then(({ data }) => {
    dispatchSetFoodParty(data)
    if (timer) clearTimeout(timer)
    timer = setTimeout(fetchFoodParty, data.remainingTime * 1000)
    if (interval) clearInterval(interval)
    interval = setInterval(dispatchDecrementRemainingTime, 1000)
  })
