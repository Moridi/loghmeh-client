import { SET_FOOD_PARTY, DECREMENT_REMAINING_TIME } from './foodparty.action'

const initialState = {
  remainingTime: 0,
  foods: [],
}

const reducers = {
  [SET_FOOD_PARTY]: (state, payload) => ({ ...state, ...payload }),

  [DECREMENT_REMAINING_TIME]: state => ({
    ...state,
    remainingTime: state.remainingTime >= 1 ? state.remainingTime - 1 : 0,
  }),
}

export default (state = initialState, { type, payload }) =>
  reducers[type] ? reducers[type](state, payload) : state
