// modules
import { createAction } from 'redux-actions'
// setup
import { dispatch } from '../../setup/redux'

export const SET_FOOD_PARTY = 'SET_FOOD_PARTY'
export const dispatchSetFoodParty = payload =>
  dispatch(createAction(SET_FOOD_PARTY)(payload))

export const DECREMENT_REMAINING_TIME = 'DECREASE_REMAINING_TIME'
export const dispatchDecrementRemainingTime = () =>
  dispatch(createAction(DECREMENT_REMAINING_TIME)())
