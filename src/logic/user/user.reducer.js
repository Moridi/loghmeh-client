import * as R from 'ramda'
import { getState } from '../../setup/redux'
// actions
import {
  SET_USER_DATA,
  SET_USER_TOKEN,
  ADD_TO_CART,
  REMOVE_FROM_CART,
  INCREASE_BALANCE,
  RESET_USER_DATA,
} from './user.action'

const initialState = {
  token: '',
  username: '',
  firstName: '',
  lastName: '',
  phone: '',
  email: '',
  balance: 0,
  cart: [], // [{ foodId, restaurantId, foodName, price, count }]
}

export const usernameView = () =>
  R.path(['main', 'user', 'username'], getState())

export const userTokenView = () => R.path(['main', 'user', 'token'], getState())

export const userCartView = () => R.path(['main', 'user', 'cart'], getState())

const reducers = {
  [SET_USER_DATA]: (state, data) => ({ ...state, ...data }),

  [RESET_USER_DATA]: R.always(initialState),

  [SET_USER_TOKEN]: (state, token) => ({ ...state, token }),

  [ADD_TO_CART]: (state, food) => {
    if (R.find(R.propEq('foodId', food.foodId), state.cart))
      return {
        ...state,
        cart: state.cart.map(item =>
          item.foodId === food.foodId
            ? { ...item, count: item.count + food.count }
            : item,
        ),
      }
    return { ...state, cart: [...state.cart, food] }
  },
  [REMOVE_FROM_CART]: (state, { foodId, count }) => {
    const food = R.find(R.propEq('foodId', foodId), state.cart)
    if (!food) return state
    if (food.count <= count)
      return {
        ...state,
        cart: R.reject(R.propEq('foodId', foodId), state.cart),
      }
    return {
      ...state,
      cart: state.cart.map(item =>
        item.foodId === foodId ? { ...item, count: item.count - count } : item,
      ),
    }
  },
  [INCREASE_BALANCE]: (state, amount) =>
    R.over(R.lensProp('balance'), R.add(amount), state),
}

export default (state = initialState, { type, payload }) =>
  reducers[type] ? reducers[type](state, payload) : state
