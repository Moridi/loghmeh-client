// modules
import * as R from 'ramda'
// actions
import { dispatchAddToCart } from './user.action'
import { dispatchSetSnackbar } from '../../app/components/snackbar/snackbar.actions'
// views
import { userCartView } from './user.reducer'
// helpers
import errCodes from '../../setup/errCodes'

export const addToCart = (food, count) => {
  if (
    R.find(
      R.complement(R.propEq('restaurantId', food.restaurantId)),
      userCartView(),
    )
  ) {
    dispatchSetSnackbar(errCodes.IMPROPER_RESTAURANT, 'error')
    return false
  }
  dispatchAddToCart(food, count)
  return true
}
