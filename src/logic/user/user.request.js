import {
  sleep,
  sendGet,
  sendPost,
  sendPut,
  withLoading,
} from '../../setup/request'
// actions
import {
  dispatchSetUserData,
  dispatchIncreaseBalance,
  dispatchSetUserToken,
  dispatchResetUserData,
} from './user.action'
import { dispatchSetSnackbar } from '../../app/components/snackbar/snackbar.actions'
import { dispatchResetFoodDetails } from '../../app/components/foodDetails/foodDetails.action'
import { dispatchSetUserCartIsOpen } from '../../app/components/userCart/userCart.action'
import { dispatchSetSignupData } from '../../app/components/signup/signup.action'
import { dispatchSetOrders } from '../orders/orders.action'
// requests
import { fetchUserOrders } from '../orders/orders.request'
import { fetchFoodParty } from '../foodparty/foodparty.request'
// helpers
import {
  setToken,
  getTokenFromStorage,
  removeToken,
} from '../../helper/functions/token'
import { navigate } from '../../setup/history'
import { fetchRestaurants } from '../restaurants/restaurants.request'
import { userCartView } from './user.reducer'

const afterUserLogin = () => {
  navigate('home')
  return withLoading(() =>
    Promise.all([fetchRestaurants(), fetchFoodParty(), fetchUserOrders()]),
  )()
}

const getUserData = () =>
  sendGet(`/user`).then(({ data }) => dispatchSetUserData(data))

export const signupUser = data =>
  sendPost('/signup', null, data).then(({ data: token }) => {
    dispatchSetSnackbar('ثبت نام با موفقیت انجام شد')
    setToken(token)
    getUserData().then(afterUserLogin)
  })

export const loginUser = ({ username, password }) =>
  sendPost('/login', null, { username, password }).then(({ data: token }) => {
    setToken(token)
    getUserData().then(afterUserLogin)
  })

export const loginWithGoogle = withLoading(
  ({ token, email, firstName, lastName }) =>
    sendPost('/login/google', null, { token }).then(({ data: jwtToken }) => {
      if (!jwtToken) {
        dispatchSetSignupData({ email, firstName, lastName })
        navigate('signup')
        return
      }

      setToken(jwtToken)
      getUserData().then(afterUserLogin)
    }),
)

export const logoutUser = () => {
  removeToken()
  dispatchResetUserData()
  dispatchResetFoodDetails()
  dispatchSetUserCartIsOpen(false)
  dispatchSetOrders([])
  window.location.reload()
}

export const checkUserToken = () =>
  sleep(1300).then(() => {
    const token = getTokenFromStorage()
    if (!token) {
      navigate('login')
      return
    }

    dispatchSetUserToken(token)
    getUserData().then(afterUserLogin).catch(logoutUser)
  })

export const chargeAccount = withLoading(amount =>
  sendPut('/user', { amount }).then(() => {
    dispatchSetSnackbar('حساب شما با موفقیت شارژ شد')
    dispatchIncreaseBalance(amount)
  }),
)

export const finalizeOrder = withLoading(() =>
  sendPost('/cart/finalizeOrder', userCartView())
    .then(() => dispatchSetUserData({ cart: [] }))
    .then(getUserData)
    .then(() => dispatchSetUserCartIsOpen(false))
    .then(() => dispatchSetSnackbar('سفارش شما با موفقیت ثبت شد'))
    .then(fetchUserOrders)
    .then(fetchFoodParty),
)
