// modules
import { createAction } from 'redux-actions'
// setup
import { dispatch } from '../../setup/redux'

export const SET_USER_DATA = 'SET_USER_DATA'
export const dispatchSetUserData = (...args) =>
  dispatch(createAction(SET_USER_DATA)(...args))

export const RESET_USER_DATA = 'RESET_USER_DATA'
export const dispatchResetUserData = (...args) =>
  dispatch(createAction(RESET_USER_DATA)(...args))

export const SET_USER_TOKEN = 'SET_USER_TOKEN'
export const dispatchSetUserToken = (...args) =>
  dispatch(createAction(SET_USER_TOKEN)(...args))

export const ADD_TO_CART = 'ADD_TO_CART'
export const dispatchAddToCart = (...args) =>
  dispatch(
    createAction(ADD_TO_CART, (food, count) => ({
      ...food,
      count,
    }))(...args),
  )

export const REMOVE_FROM_CART = 'REMOVE_FROM_CART'
export const dispatchRemoveFromCart = (...args) =>
  dispatch(
    createAction(REMOVE_FROM_CART, (foodId, count) => ({ foodId, count }))(
      ...args,
    ),
  )

export const INCREASE_BALANCE = 'INCREASE_BALANCE'
export const dispatchIncreaseBalance = amount =>
  dispatch(createAction(INCREASE_BALANCE)(amount))
