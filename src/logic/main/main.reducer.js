// modules
import { combineReducers } from 'redux'
// reducers
import user from '../user/user.reducer'
import orders from '../orders/orders.reducer'
import restaurants from '../restaurants/restaurants.reducer'
import foodParty from '../foodparty/foodparty.reducer'

export default combineReducers({
  user,
  orders,
  restaurants,
  foodParty,
})
