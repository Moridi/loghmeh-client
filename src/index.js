import React from 'react'
import ReactDOM from 'react-dom'
import App from './app/app.jsx'
import { checkUserToken } from './logic/user/user.request'
// styles
import './index.css'

checkUserToken()

ReactDOM.render(<App />, document.getElementById('root'))
