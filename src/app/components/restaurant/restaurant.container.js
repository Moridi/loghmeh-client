import { connect } from 'react-redux'
import Restaurant from './restaurant'
import { dispatchSetRestaurantData } from './restaurant.action'
import { dispatchOpenFoodDetails } from '../foodDetails/foodDetails.action'
import { getRestaurantById } from '../../../logic/restaurants/restaurants.request'

const mapStateToProps = state => {
  const { name, logo, menu } = state.view.restaurant
  return {
    name: name || 'در حال دریافت',
    logo,
    foods: menu || [],
  }
}

const mapDispatchToProps = (_, { id }) => ({
  onFoodClick: (restaurantId, restaurantName) => food => () =>
    dispatchOpenFoodDetails({ ...food, restaurantName, restaurantId }),
  onMount: () => getRestaurantById(id).then(dispatchSetRestaurantData),
})

export default connect(mapStateToProps, mapDispatchToProps)(Restaurant)
