// modules
import { createAction } from 'redux-actions'
// setup
import { dispatch } from '../../../setup/redux'

export const dispatchSetRestaurantData = payload =>
  dispatch(createAction('SET_RESTAURANT_DATA')(payload))

export const dispatchResetRestaurant = () =>
  dispatch(createAction('RESET_RESTAURANT')())
