import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
// components
import Footer from '../../../helper/components/footer/footer'
import RestaurantHeader from './components/header'
import Menu from './components/menu'
import Cart from '../../../helper/components/cart/cart.container'

// style
import './restaurant.css'

const Restaurant = ({ id, name, logo, foods, onMount, onFoodClick }) => {
  useEffect(() => {
    onMount()
  }, [])

  return (
    <div className='restaurant-container'>
      <RestaurantHeader logo={logo} name={name} />
      <div className='restaurant-body'>
        <Menu foods={foods} onFoodClick={onFoodClick(id, name)} />
        <div className='body-divider'></div>
        <Cart />
      </div>
      <Footer />
    </div>
  )
}

Restaurant.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  foods: PropTypes.array.isRequired,
  logo: PropTypes.string,
}

Restaurant.defaultProps = {
  logo: '',
}

export default Restaurant
