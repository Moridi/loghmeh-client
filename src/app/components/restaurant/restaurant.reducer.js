const initialState = {}

const reducers = {
  SET_RESTAURANT_DATA: (state, data) => data,

  RESET_RESTAURANT: () => initialState,
}

export default (state = initialState, { type, payload }) =>
  reducers[type] ? reducers[type](state, payload) : state
