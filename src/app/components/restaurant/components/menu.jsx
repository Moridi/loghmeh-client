import React from 'react'
import PropTypes from 'prop-types'
// components
import { toPersian } from '../../../../helper/functions/utils'
// style
import './menu.css'

const MenuItem = ({ name, price, popularity, image, onClick }) => (
  <div className='menu-item'>
    <img src={image} alt='عکس غذا' />
    <div className='menu-item-name'>
      <p>{name}</p>
      <div className='star-icon'>
        <p>{toPersian(popularity * 5)}</p>
        <svg className='icon'>
          <use xlinkHref='#star' />
        </svg>
      </div>
    </div>

    <p className='menu-item-price' dir='rtl'>
      {toPersian(price)} تومان
    </p>

    <button className='btn submit-yellow' onClick={onClick}>
      افزودن به سبد خرید
    </button>
  </div>
)

MenuItem.propTypes = {
  name: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  popularity: PropTypes.number.isRequired,
  image: PropTypes.string.isRequired,
}

const Menu = ({ foods, onFoodClick }) => {
  return (
    <div className='menu-container'>
      <div className='menu-header'>
        <p>منوی غذا</p>
        <div className='title-underline'></div>
      </div>
      <div className='menu-body'>
        {foods.map(food => (
          <MenuItem {...food} key={food.name} onClick={onFoodClick(food)} />
        ))}
      </div>
    </div>
  )
}

Menu.propTypes = {
  foods: PropTypes.array.isRequired,
  onFoodClick: PropTypes.func.isRequired,
}

export default Menu
