import React from 'react'
// components
// style
import './header.css'

const RestaurantHeader = ({ name, logo }) => (
  <div className='restaurant-header'>
    <div className='restaurant-top'></div>
    <div className='restaurant-logo'>
      {logo ? (
        <img className='restaurant-image' src={logo} alt={logo} />
      ) : (
        <div className='blank-logo' />
      )}
      <p className='restaurant-name'>{name}</p>
    </div>
  </div>
)

export default RestaurantHeader
