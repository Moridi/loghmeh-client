// modules
import React from 'react'
// components
import Ellipsis from '../loading/components/ellipsis'
// style
import './loadingPage.css'

export default () => (
  <div className='loading-page'>
    <img class='loading-logo' src='/images/LOGO.png' alt='لقمه' />
    <Ellipsis />
  </div>
)
