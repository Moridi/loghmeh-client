// modules
import { connect } from 'react-redux'
// components
import UserCart from './userCart'
// actions
import { dispatchSetUserCartIsOpen } from './userCart.action'

const mapStateToProps = state => ({
  isOpen: state.view.userCart.isOpen,
})

const mapDispatchToProps = () => ({
  onClose: () => dispatchSetUserCartIsOpen(false),
})

export default connect(mapStateToProps, mapDispatchToProps)(UserCart)
