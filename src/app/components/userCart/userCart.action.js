// modules
import { createAction } from 'redux-actions'
// setup
import { dispatch } from '../../../setup/redux'

export const SET_USER_CART_IS_OPEN = 'SET_USER_CART_IS_OPEN'
export const dispatchSetUserCartIsOpen = (...args) =>
  dispatch(createAction(SET_USER_CART_IS_OPEN)(...args))
