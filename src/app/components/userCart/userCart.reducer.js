const initialState = {
  isOpen: false,
}

const reducers = {
  SET_USER_CART_IS_OPEN: (state, isOpen) => ({ ...state, isOpen }),
}

export default (state = initialState, { type, payload }) =>
  reducers[type] ? reducers[type](state, payload) : state
