import React from 'react'
// components
import Dialog from '../../../helper/components/dialog/dialog'
import Cart from '../../../helper/components/cart/cart.container'

const UserCart = ({ isOpen, onClose }) => (
  <Dialog isOpen={isOpen} onClose={onClose}>
    <Cart />
  </Dialog>
)

export default UserCart
