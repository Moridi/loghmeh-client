// modules
import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'
// components
import Ellipsis from './components/ellipsis'
import Dialog from '../../../helper/components/dialog/dialog'

// styles
const useStyles = makeStyles({
  root: {
    width: 180,
    height: 150,
    borderRadius: 25,
  },
  loading: {
    width: '100%',
    height: '100%',

    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    marginTop: 20,
    color: 'gray',
  },
})

const Loading = ({ isLoading }) => {
  const classes = useStyles()
  return (
    <Dialog isOpen={isLoading} className={classes.root}>
      <div className={classes.loading}>
        <Ellipsis />
        <span className={classes.text}>لطفا صبر کنید</span>
      </div>
    </Dialog>
  )
}

Loading.propTypes = {
  isLoading: PropTypes.bool,
}

Loading.defaultProps = {
  isLoading: false,
}

export default Loading
