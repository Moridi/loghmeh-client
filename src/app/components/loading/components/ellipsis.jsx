// modules
import React from 'react'
// styles
import './ellipsis.scss'

const Ellipsis = () => (
  <div className='circles'>
    <span className='circle circle-1' />
    <span className='circle circle-2' />
    <span className='circle circle-3' />
    <span className='circle circle-4' />
  </div>
)

export default Ellipsis
