// modules
import { connect } from 'react-redux'
// components
import Loading from './loading'

const mapStateToProps = state => ({
  isLoading: state.view.loading.isLoading,
})

export default connect(mapStateToProps)(Loading)
