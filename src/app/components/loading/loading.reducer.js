// actions
import { SET_IS_LOADING } from './loading.action'

// state
const initialState = {
  isLoading: false,
}

// reducers
const reducers = {
  [SET_IS_LOADING]: (state, isLoading) => ({ ...state, isLoading }),
}

export default (state = initialState, { type, payload }) =>
  reducers[type] ? reducers[type](state, payload) : state
