// modules
import { createAction } from 'redux-actions'
// setup
import { dispatch } from '../../../setup/redux'

export const SET_IS_LOADING = 'SET_IS_LOADING'

export const dispatchSetIsLoading = value =>
  dispatch(createAction(SET_IS_LOADING)(value))
