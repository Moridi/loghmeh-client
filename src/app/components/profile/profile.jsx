import React, { useState } from 'react'
import PropTypes from 'prop-types'
// components
import Header from './components/header'
import Tabs from './components/tabs'
import ChargePanel from './components/charge'
import OrdersPanel from './components/orders'
import Footer from '../../../helper/components/footer/footer'
// style
import './profile.css'

const Profile = ({ orders, onCharge, ...headerProps }) => {
  const [tab, setTab] = useState('charge') // charge | orders

  return (
    <div className='profile-container'>
      <Header {...headerProps} />
      <div className='profile-body'>
        <Tabs tab={tab} changeTab={setTab} />
        {tab === 'charge' ? (
          <ChargePanel onCharge={onCharge} />
        ) : (
          <OrdersPanel orders={orders} />
        )}
      </div>
      <Footer />
    </div>
  )
}

Profile.propTypes = {
  orders: PropTypes.array.isRequired,
  onCharge: PropTypes.func.isRequired,
}

export default Profile
