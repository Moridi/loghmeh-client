// modules
import * as R from 'ramda'
import { connect } from 'react-redux'
// components
import Profile from './profile'
// requests
import { chargeAccount } from '../../../logic/user/user.request'
import { dispatchSetSnackbar } from '../snackbar/snackbar.actions'

const mapStateToProps = state => {
  const { firstName, lastName, email, phone, balance } = state.main.user
  return {
    name: `${firstName} ${lastName}`,
    email,
    phone,
    balance,
    orders: R.sortBy(R.prop('id'), state.main.orders),
  }
}

const mapDispatchToProps = () => ({
  onCharge: amount => {
    if (amount <= 0) {
      dispatchSetSnackbar('مقدار وارد شده مجاز نمی‌باشد', 'error')
      return
    }
    chargeAccount(amount)
  },
})

export default connect(mapStateToProps, mapDispatchToProps)(Profile)
