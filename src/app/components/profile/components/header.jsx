import React from 'react'
import PropTypes from 'prop-types'
// helper
import { toPersian } from '../../../../helper/functions/utils'
// style
import '../profile.css'

const ProfileHeader = ({ name, email, phone, balance }) => (
  <div className='profile-header'>
    <div className='profile-header-contact'>
      <div className='profile-header-contact-text'>
        <p>{toPersian(phone)}</p>
        <span className='flaticon-phone'></span>
      </div>
      <div className='profile-header-contact-text'>
        <p>{email}</p>
        <span className='flaticon-mail'></span>
      </div>
      <div className='profile-header-contact-text'>
        <p dir='rtl'>{toPersian(balance)} تومان</p>
        <span className='flaticon-card'></span>
      </div>
    </div>
    <div className='profile-header-name'>
      <p>{name}</p>
      <span className='flaticon-account'></span>
    </div>
  </div>
)

ProfileHeader.propTypes = {
  name: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  phone: PropTypes.string.isRequired,
  balance: PropTypes.number.isRequired,
}

export default ProfileHeader
