import React from 'react'
import PropTypes from 'prop-types'
// components
import Dialog from '../../../../helper/components/dialog/dialog'
// helper
import { toPersian } from '../../../../helper/functions/utils'
// style
import './orderFactor.css'

const OrderFactor = ({ isOpen, order, onClose }) => {
  if (!isOpen) return null
  const { restaurantName, foods } = order
  const totalPrice = foods.reduce(
    (sum, { count, price }) => sum + count * price,
    0,
  )
  return (
    <Dialog isOpen={isOpen} onClose={onClose} className='factor-dialog'>
      <div class='factor-container'>
        <p className='factor-restaurant-name'>{restaurantName}</p>
        <span className='factor-restaurant-splitter'></span>
        <table className='factor-table'>
          <tr>
            <th>قیمت</th>
            <th>تعداد</th>
            <th>نام غذا</th>
            <th>ردیف</th>
          </tr>
          {foods.map(({ foodName, count, price }, index) => (
            <tr>
              <td>{toPersian(price * count)}</td>
              <td>{toPersian(count)}</td>
              <td>{foodName}</td>
              <td>{toPersian(index + 1)}</td>
            </tr>
          ))}
        </table>
        <p className='factor-total-price' dir='rtl'>{`جمع کل: ${toPersian(
          totalPrice,
        )} تومان`}</p>
      </div>
    </Dialog>
  )
}

OrderFactor.propTypes = {
  order: PropTypes.shape({}),
}

OrderFactor.defaultProps = {
  order: {},
}

export default OrderFactor
