import React from 'react'
import PropTypes from 'prop-types'
// style
import '../profile.css'

const ProfileTabs = ({ tab, changeTab }) => (
  <div className='profile-tabs'>
    <div className='tabs-divider'></div>
    <div className='btn-group full' role='group'>
      <button
        className={`btn ${tab === 'charge' ? 'tab-selected' : 'tab-normal'}`}
        onClick={() => changeTab('charge')}
      >
        افزایش اعتبار
      </button>
      <button
        className={`btn ${tab === 'orders' ? 'tab-selected' : 'tab-normal'}`}
        onClick={() => changeTab('orders')}
      >
        سفارش ها
      </button>
    </div>
  </div>
)

ProfileTabs.propTypes = {
  tab: PropTypes.string.isRequired,
  changeTab: PropTypes.func.isRequired,
}

export default ProfileTabs
