import React, { useState } from 'react'
import PropTypes from 'prop-types'
// components
import Input from '../../../../helper/components/input/input'
// style
import '../profile.css'

const ProfileCharge = ({ onCharge }) => {
  const [amount, setAmount] = useState('')
  return (
    <div className='body-panel charge-panel'>
      <button className='btn charge-button' onClick={() => onCharge(amount)}>
        افزایش
      </button>
      <Input
        value={amount}
        type='number'
        placeholder='میزان افزایش اعتبار'
        className='charge-input'
        onChange={setAmount}
      />
    </div>
  )
}

ProfileCharge.propTypes = {
  onCharge: PropTypes.func.isRequired,
}

export default ProfileCharge
