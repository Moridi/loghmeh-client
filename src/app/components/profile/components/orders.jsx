import React, { useState } from 'react'
import PropTypes from 'prop-types'
// components
import OrderFactor from './orderFactor'
// style
import '../profile.css'

const StatusComp = ({ status, onClick }) => {
  if (status === 'FINDING_DELIVERY')
    return <p className='status-finding-delivery center'>در جست‌و‌جوی پیک</p>
  if (status === 'DELIVERY_FOUND')
    return <p className='status-delivering center'>پیک در مسیر</p>
  return (
    <button className='btn submit-yellow center' onClick={onClick}>
      مشاهده فاکتور
    </button>
  )
}

const OrderItem = ({ row, status, restaurantName, onClick }) => (
  <div className='order-item'>
    <div className='order-item-status center'>
      <StatusComp status={status} onClick={onClick} />
    </div>
    <div className='order-item-name center'>{restaurantName}</div>
    <div className='order-item-row center'>{row}</div>
  </div>
)

OrderItem.propTypes = {
  row: PropTypes.number.isRequired,
  status: PropTypes.string.isRequired,
  restaurantName: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
}

const ProfileOrders = ({ orders }) => {
  const [selectedOrder, setSelectedOrder] = useState()

  return (
    <div className='body-panel orders-panel'>
      <OrderFactor
        isOpen={!!selectedOrder}
        order={selectedOrder}
        onClose={() => setSelectedOrder(null)}
      />
      {orders.length ? (
        orders.map(({ status, restaurantName, foods }, index) => (
          <OrderItem
            row={index + 1}
            status={status}
            restaurantName={restaurantName}
            onClick={() => setSelectedOrder({ restaurantName, foods })}
          />
        ))
      ) : (
        <p className='orders-empty center no-select'>سفارشی وجود ندارد</p>
      )}
    </div>
  )
}

ProfileOrders.propTypes = {
  orders: PropTypes.arrayOf.isRequired,
}

export default ProfileOrders
