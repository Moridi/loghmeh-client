import React from 'react'
import PropTypes from 'prop-types'
// helper
import { toPersian } from '../../../helper/functions/utils'
// style
import './navigator.css'

const Navigator = ({
  hidden,
  options, // signup, login, exit, profile, cart, home
  cartBadge,
  rootClassName,
  onSignupClick,
  onLoginClick,
  onExitClick,
  onProfileClick,
  onCartClick,
  onHomeClick,
}) => {
  if (hidden) return null
  return (
    <div className={rootClassName}>
      <div className='left-nav-links'>
        {options.includes('signup') ? (
          <span className='login center' onClick={onSignupClick}>
            ثبت نام
          </span>
        ) : null}
        {options.includes('login') ? (
          <span className='login center' onClick={onLoginClick}>
            ورود
          </span>
        ) : null}
        {options.includes('exit') ? (
          <span className='exit' onClick={onExitClick}>
            خروج
          </span>
        ) : null}
        {options.includes('profile') ? (
          <span className='profile' onClick={onProfileClick}>
            حساب کاربری
          </span>
        ) : null}
        {options.includes('cart') ? (
          <span className='flaticon-smart-cart cart' onClick={onCartClick}>
            <p className='nav-cart-badge center'>{toPersian(cartBadge)}</p>
          </span>
        ) : null}
      </div>
      {options.includes('home') ? (
        <span className='home-link center' onClick={onHomeClick}>
          <img src='../../../images/LOGO.png' alt='لقمه' />
        </span>
      ) : null}
    </div>
  )
}

Navigator.propTypes = {
  hidden: PropTypes.bool,
  options: PropTypes.array.isRequired,
  cartBadge: PropTypes.number,
  rootClassName: PropTypes.string,
  onLoginClick: PropTypes.func,
  onSignupClick: PropTypes.func,
  onExitClick: PropTypes.func,
  onProfileClick: PropTypes.func,
  onCartClick: PropTypes.func,
  onHomeClick: PropTypes.func,
}

Navigator.defaultProps = {
  hidden: false,
  cartBadge: 0,
  rootClassName: '',

  onLoginClick: () => {},
  onSignupClick: () => {},
  onExitClick: () => {},
  onProfileClick: () => {},
  onCartClick: () => {},
  onHomeClick: () => {},
}

export default Navigator
