// modules
import { connect } from 'react-redux'
// components
import Navigator from './navigator'
// actions
import { dispatchSetUserCartIsOpen } from '../userCart/userCart.action'
// helpers
import { navigate } from '../../../setup/history'
import { logoutUser } from '../../../logic/user/user.request'

const mapStateToProps = (state, { page }) => ({
  options: page.startsWith('restaurant')
    ? ['exit', 'profile', 'cart', 'home']
    : page === 'home'
    ? ['exit', 'profile', 'cart']
    : page === 'profile'
    ? ['exit', 'cart', 'home']
    : page === 'login'
    ? ['signup']
    : ['login'],
  rootClassName:
    page === 'signup' || page === 'login' ? 'signup-nav' : 'navigator',
  cartBadge: (state.main.user.cart || []).length,
  hidden: page === 'loading',
})

const mapDispatchToProps = () => ({
  onCartClick: () => dispatchSetUserCartIsOpen(true),
  onLoginClick: () => navigate('login'),
  onSignupClick: () => navigate('signup'),
  onProfileClick: () => navigate('profile'),
  onHomeClick: () => navigate('home'),
  onExitClick: logoutUser,
})

export default connect(mapStateToProps, mapDispatchToProps)(Navigator)
