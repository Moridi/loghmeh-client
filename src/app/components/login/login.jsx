import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
// components
import Input from '../../../helper/components/input/input'
import Footer from '../../../helper/components/footer/footer'
// style
import '../signup/signup.css'
import './login.css'

const Login = ({ onLogin, onGoogleLogin }) => {
  const [values, setValues] = useState({})
  const onChange = key => value => {
    setValues({ ...values, [key]: value })
  }
  useEffect(() => {
    window.gapi.signin2.render('google-signin2', {
      scope: 'profile email',
      width: 240,
      height: 50,
      longtitle: true,
      theme: 'dark',
      onsuccess: onGoogleLogin,
      onfailure: console.log,
    })
  }, [])

  return (
    <div className='signup-container'>
      <div className='signup-body'>
        <img
          className='home-cover signup-background'
          src='/images/Cover.jpg'
          alt='background'
        ></img>
        <img className='signup-logo' src='/images/LOGO.png' alt='لقمه' />
        <div className='signup-panel login-panel'>
          <p className='signup-title'>ایمیل</p>
          <Input
            value={values.email}
            placeholder='مثال: example@loghmeh.ir'
            className='signup-input'
            onChange={onChange('email')}
          />
          <p className='signup-title'>رمز عبور</p>
          <Input
            value={values.password}
            placeholder='رمز عبور'
            className='signup-input'
            type='password'
            onChange={onChange('password')}
          />
        </div>
        <button className='btn signup-button' onClick={() => onLogin(values)}>
          ورود
        </button>
        <div id='google-signin2' className='google-signin'></div>
      </div>
      <Footer />
    </div>
  )
}

Login.propTypes = {
  onLogin: PropTypes.func.isRequired,
}

export default Login
