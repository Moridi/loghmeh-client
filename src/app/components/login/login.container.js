// modules
import { connect } from 'react-redux'
// components
import Login from './login'
// requests
import { dispatchSetSnackbar } from '../snackbar/snackbar.actions'
import { loginUser, loginWithGoogle } from '../../../logic/user/user.request'

const mapDispatchToProps = () => ({
  onLogin: values => {
    const { email, password } = values
    if (!email || !password)
      dispatchSetSnackbar('لطفا تمام مقادیر را وارد کنید', 'error')
    loginUser({ username: email, password })
  },
  onGoogleLogin: googleUser => {
    const profile = googleUser.getBasicProfile()
    loginWithGoogle({
      token: googleUser.getAuthResponse().id_token,
      email: profile.getEmail(),
      firstName: profile.getGivenName(),
      lastName: profile.getFamilyName(),
    })

    if (window.gapi) {
      const auth2 = window.gapi.auth2.getAuthInstance()
      if (auth2 != null) auth2.signOut().then(auth2.disconnect())
    }
  },
})

export default connect(null, mapDispatchToProps)(Login)
