// modules
import { createAction } from 'redux-actions'
// setup
import { dispatch } from '../../../setup/redux'

export const dispatchOpenFoodDetails = payload =>
  dispatch(createAction('OPEN_FOOD_DETAILS')(payload))

export const dispatchResetFoodDetails = () =>
  dispatch(createAction('RESET_FOOD_DETAILS')())
