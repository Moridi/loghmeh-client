const initialState = null

const reducers = {
  OPEN_FOOD_DETAILS: (state, food) => food,

  RESET_FOOD_DETAILS: () => null,
}

export default (state = initialState, { type, payload }) =>
  reducers[type] ? reducers[type](state, payload) : state
