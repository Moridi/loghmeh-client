import React, { useState } from 'react'
import PropTypes from 'prop-types'
// components
import Dialog from '../../../helper/components/dialog/dialog'
// helper
import { toPersian } from '../../../helper/functions/utils'
// style
import './foodDetails.css'

const FoodDetails = ({
  isOpen,
  name,
  description,
  popularity,
  restaurantName,
  price,
  image,
  // foodparty
  isParty,
  oldPrice,
  count,
  // handlers
  onAddToCart,
  onClose,
}) => {
  const [quantity, setQuantity] = useState(0)
  const increase = value => () =>
    setQuantity(oldQuantity =>
      oldQuantity + value >= 0 ? oldQuantity + value : oldQuantity,
    )
  const disabled = !quantity || (isParty && quantity > count)

  const onCloseDialog = () => {
    setQuantity(0)
    onClose()
  }

  const addToCart = () => {
    setQuantity(0)
    onAddToCart(quantity)
  }

  return (
    <Dialog isOpen={isOpen} className='food-details' onClose={onCloseDialog}>
      <p className='food-details-restaurant'>{restaurantName}</p>
      <div className='food-details-body'>
        <div className='food-details-info'>
          <div className='food-details-name'>
            <div className='details-star'>
              <p>{toPersian(popularity * 5)}</p>
              <svg className='icon'>
                <use xlinkHref='#star' />
              </svg>
            </div>
            <p className='food-details-name-text'>{name}</p>
          </div>
          <p className='food-details-description'>{description}</p>
          <div className='food-details-prices'>
            <p dir='rtl'>{toPersian(price)} تومان</p>
            {isParty ? (
              <p className='food-details-old-price'>{toPersian(oldPrice)}</p>
            ) : null}
          </div>
        </div>
        <img src={image} alt={name}></img>
      </div>
      <div className='food-details-splitter' />
      <div className='food-details-footer'>
        <span>
          <button
            className='btn submit-blue'
            onClick={addToCart}
            disabled={disabled}
          >
            افزودن به سبد خرید
          </button>
          <div className='food-details-quantity'>
            <div
              className='flaticon-minus pointer'
              onClick={increase(-1)}
            ></div>
            <p className='no-select'>{toPersian(quantity)}</p>
            <div className='flaticon-plus pointer' onClick={increase(1)}></div>
          </div>
        </span>
        {isParty ? (
          <p className='food-details-count center'>
            {count ? `موجودی: ${toPersian(count)}` : 'ناموجود'}
          </p>
        ) : (
          <span />
        )}
      </div>
    </Dialog>
  )
}

FoodDetails.propTypes = {
  isOpen: PropTypes.bool,
  name: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  popularity: PropTypes.number.isRequired,
  restaurantName: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  image: PropTypes.string.isRequired,
  isParty: PropTypes.bool,
  oldPrice: PropTypes.number,
  count: PropTypes.number,
  onAddToCart: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
}

FoodDetails.defaultProps = {
  isOpen: false,
  isParty: false,
  oldPrice: null,
  count: null,
}

export default FoodDetails
