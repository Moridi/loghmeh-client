// modules
import { connect } from 'react-redux'
// components
import FoodDetails from './foodDetails'
// actions
import { dispatchResetFoodDetails } from './foodDetails.action'
import { dispatchSetSnackbar } from '../snackbar/snackbar.actions'
// helpers
import { addToCart } from '../../../logic/user/user.helper'

const mapStateToProps = state => {
  const food = state.view.foodDetails
  return {
    ...food,
    isOpen: !!food,
    isParty: food && food.oldPrice !== undefined,
  }
}

const mapDispatchToProps = () => ({
  onAddToCart: (food, count) => {
    if (addToCart(food, count)) {
      dispatchSetSnackbar('با موفقیت به سبد شما اضافه شد')
      dispatchResetFoodDetails()
    }
  },
  onClose: dispatchResetFoodDetails,
})

const mergeProps = (propsFromState, propsFromDispatch, ownProps) => {
  const { onAddToCart } = propsFromDispatch
  const { restaurantId, id, name, price } = propsFromState
  return {
    ...ownProps,
    ...propsFromState,
    ...propsFromDispatch,
    onAddToCart: amount =>
      onAddToCart({ restaurantId, foodId: id, foodName: name, price }, amount),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps,
)(FoodDetails)
