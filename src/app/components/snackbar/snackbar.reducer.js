// actions
import { SET_SNACKBAR } from './snackbar.actions'

// state
const initialState = {
  snackbarIsOpen: false,
  message: '',
  type: null,
}

// reducers
const reducers = {
  [SET_SNACKBAR]: (_, payload) => payload,
}

export default (state = initialState, { type, payload }) =>
  reducers[type] ? reducers[type](state, payload) : state
