// modules
import { createAction } from 'redux-actions'
// setup
import { dispatch } from '../../../setup/redux'

export const SET_SNACKBAR = 'SET_SNACKBAR'

export const dispatchSetSnackbar = (...args) =>
  dispatch(
    createAction(SET_SNACKBAR, (message, type = 'success') => ({
      snackbarIsOpen: true,
      message,
      type,
    }))(...args),
  )

export const dispatchResetSnackbar = (...args) =>
  dispatch(
    createAction(SET_SNACKBAR, () => ({
      snackbarIsOpen: false,
      message: '',
      type: null,
    }))(...args),
  )
