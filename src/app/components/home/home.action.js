// modules
import { createAction } from 'redux-actions'
// setup
import { dispatch } from '../../../setup/redux'

export const dispatchSetSearchResults = results =>
  dispatch(createAction('SET_SEARCH_RESULTS')(results))

export const dispatchAddSearchResults = results =>
  dispatch(createAction('ADD_SEARCH_RESULTS')(results))

export const dispatchSetSearchQuery = payload =>
  dispatch(createAction('SET_SEARCH_QUERY')(payload))

export const dispatchSetShowMoreVisibility = visibility =>
  dispatch(createAction('SET_SHOW_MORE_VISIBILITY')(visibility))
