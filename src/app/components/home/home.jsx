import React from 'react'
import PropTypes from 'prop-types'
// components
import Header from './components/header'
import RestaurantCard from './components/restaurantCard'
import FoodPartyCard from './components/foodPartyCard'
import Footer from '../../../helper/components/footer/footer'
import CloseButton from '../../../helper/components/closeButton/closeButton'
// helper
import {
  ab,
  cns,
  toPersian,
  formattedSeconds,
} from '../../../helper/functions/utils'
// style
import './home.css'

const Home = ({
  restaurants,
  foodPartyFoods,
  foodPartyRemainigTime,
  showingSearchResults,
  showMoreVisibility,
  onSearchClick,
  onCancelSearch,
  onFoodPartyClick,
  onOpenRestaurant,
  onShowMoreClick,
}) => {
  return (
    <div className='home-container'>
      <Header onSearchClick={onSearchClick} />
      <p className='home-section-title' dir='rtl'>
        جشن غذا!
      </p>
      <div className='home-title-underline' />
      <p className='foodparty-timer center'>
        زمان باقی‌مانده: {toPersian(formattedSeconds(foodPartyRemainigTime))}
      </p>
      <div className='foodparty-box'>
        {foodPartyFoods.map(food => (
          <FoodPartyCard
            key={food.name}
            onClick={onFoodPartyClick(food)}
            {...food}
          />
        ))}
      </div>
      <div className='home-restaurants-title-box'>
        <p className='home-section-title' dir='rtl'>
          {showingSearchResults ? 'نتایج جست‌و‌جو' : 'رستوران‌ها'}
        </p>
        {showingSearchResults ? <CloseButton onClick={onCancelSearch} /> : null}
      </div>
      <div
        className={cns(
          'home-title-underline',
          ab('long')(showingSearchResults),
        )}
      />
      <div className='home-restaurants'>
        {restaurants.length ? (
          restaurants.map(({ id, name, logo }) => (
            <RestaurantCard
              key={id}
              name={name}
              logo={logo}
              onClick={onOpenRestaurant(id)}
            />
          ))
        ) : (
          <p className='no-restaurants'>نتیجه‌ای یافت نشد</p>
        )}
      </div>
      {showMoreVisibility ? (
        <button className='btn show-more-btn' onClick={onShowMoreClick}>
          نمایش بیشتر
        </button>
      ) : null}
      <Footer />
    </div>
  )
}

Home.propTypes = {
  restaurants: PropTypes.array.isRequired,
  foodPartyFoods: PropTypes.array.isRequired,
  foodPartyRemainigTime: PropTypes.number.isRequired,
  showMoreVisibility: PropTypes.bool.isRequired,
  onSearchClick: PropTypes.func.isRequired,
  onFoodPartyClick: PropTypes.func.isRequired,
  onOpenRestaurant: PropTypes.func.isRequired,
  onShowMoreClick: PropTypes.func.isRequired,
}

export default Home
