import React from 'react'
import PropTypes from 'prop-types'

const RestaurantCard = ({ name, logo, onClick }) => (
  <div className='rest-card'>
    <img src={logo} alt={name} className='rest-card-logo'></img>
    <p>{name}</p>
    <button className='btn submit-yellow no-shadows' onClick={onClick}>
      نمایش منو
    </button>
  </div>
)

RestaurantCard.propTypes = {
  name: PropTypes.string.isRequired,
  logo: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
}

export default RestaurantCard
