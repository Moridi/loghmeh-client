import React from 'react'
import PropTypes from 'prop-types'
// helper
import { toPersian } from '../../../../helper/functions/utils'
// style
import './foodPartyCard.css'

const FoodPartyCard = ({
  name,
  popularity,
  restaurantName,
  price,
  oldPrice,
  image,
  count,
  onClick,
}) => (
  <div className='party-card'>
    <div className='party-card-header'>
      <div className='party-card-title'>
        <p className='party-card-title-text'>{name}</p>
        <div className='star-icon'>
          <p>{toPersian(popularity * 5)}</p>
          <svg className='icon'>
            <use xlinkHref='#star' />
          </svg>
        </div>
      </div>
      <img src={image} alt={name} />
    </div>
    <div className='party-card-prices'>
      <p>{toPersian(price)}</p>
      <p className='party-card-old-price'>{toPersian(oldPrice)}</p>
    </div>
    <div className='party-card-action'>
      <button className='btn submit-blue' onClick={onClick} disabled={!count}>
        خرید
      </button>
      <p className='center'>
        {count ? `موجودی: ${toPersian(count)}` : 'ناموجود'}
      </p>
    </div>
    <div className='party-card-splitter' />
    <p className='party-card-restaurant center'>{restaurantName}</p>
  </div>
)

FoodPartyCard.propTypes = {
  name: PropTypes.string.isRequired,
  popularity: PropTypes.number.isRequired,
  restaurantName: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  oldPrice: PropTypes.number.isRequired,
  image: PropTypes.string.isRequired,
  count: PropTypes.number.isRequired,
  onClick: PropTypes.func.isRequired,
}

export default FoodPartyCard
