import React, { useState } from 'react'
// components
import Input from '../../../../helper/components/input/input'
// style
import '../home.css'

const HomeHeader = ({ onSearchClick }) => {
  const [resturantName, setRestaurantName] = useState('')
  const [foodName, setFoodName] = useState('')

  return (
    <div className='home-header'>
      <img
        className='home-cover'
        src='/images/Cover.jpg'
        alt='home cover'
      ></img>
      <span className='home-cover-color'></span>
      <img
        className='home-header-logo'
        src='/images/LOGO.png'
        alt='loghmeh logo'
      ></img>
      <p className='home-header-text'>
        اولین و بزرگ‌ترین وب‌سایت سفارش آنلاین غذا در دانشگاه تهران
      </p>
      <div className='home-search-box'>
        <button
          className='btn submit-yellow no-shadows'
          onClick={() => onSearchClick(resturantName, foodName)}
          disabled={!resturantName && !foodName}
        >
          جست‌و‌جو
        </button>
        <Input
          className='home-search-input'
          placeholder='نام رستوران'
          value={resturantName}
          onChange={setRestaurantName}
        />
        <Input
          className='home-search-input'
          placeholder='نام غذا'
          value={foodName}
          onChange={setFoodName}
        />
      </div>
    </div>
  )
}

export default HomeHeader
