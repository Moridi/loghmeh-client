const initialState = {
  searchResults: null,
  searchQuery: {
    restaurantName: '',
    foodName: '',
  },
  showMoreVisibility: false,
}

const reducers = {
  SET_SEARCH_RESULTS: (state, searchResults) => ({ ...state, searchResults }),

  ADD_SEARCH_RESULTS: (state, results) => ({
    ...state,
    searchResults: [...(state.searchResults || []), ...results],
  }),

  SET_SEARCH_QUERY: (state, searchQuery) => ({ ...state, searchQuery }),

  SET_SHOW_MORE_VISIBILITY: (state, showMoreVisibility) => ({
    ...state,
    showMoreVisibility,
  }),
}

export default (state = initialState, { type, payload }) =>
  reducers[type] ? reducers[type](state, payload) : state
