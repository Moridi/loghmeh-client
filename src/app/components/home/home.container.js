// modules
import { connect } from 'react-redux'
// components
import Home from './home'
// actions
import { dispatchResetRestaurant } from '../restaurant/restaurant.action'
import { dispatchOpenFoodDetails } from '../foodDetails/foodDetails.action'
import {
  dispatchSetSearchResults,
  dispatchSetSearchQuery,
  dispatchSetShowMoreVisibility,
} from './home.action'
import { navigate } from '../../../setup/history'
// requests
import {
  searchRestaurants,
  fetchRestaurants,
} from '../../../logic/restaurants/restaurants.request'
import { withLoading } from '../../../setup/request'

const mapStateToProps = ({
  main: { restaurants, foodParty },
  view: { home },
}) => ({
  restaurants: home.searchResults || restaurants,
  foodPartyRemainigTime: foodParty.remainingTime,
  foodPartyFoods: foodParty.foods,
  showingSearchResults: !!home.searchResults,
  searchQuery: home.searchQuery,
  showMoreVisibility: home.showMoreVisibility,
})

const mapDispatchToProps = () => ({
  onSearchClick: (restaurantName, foodName) => {
    dispatchSetSearchResults(null)
    searchRestaurants(restaurantName, foodName)
    dispatchSetSearchQuery({ restaurantName, foodName })
  },
  onCancelSearch: () => {
    dispatchSetSearchResults(null)
    dispatchSetShowMoreVisibility(true)
    dispatchSetSearchQuery({ restaurantName: '', foodName: '' })
  },
  onOpenRestaurant: restaurantId => () => {
    dispatchResetRestaurant()
    navigate(`/restaurant/${restaurantId}`)
  },
  onFoodPartyClick: food => () => dispatchOpenFoodDetails(food),
})

const mergeProps = (propsFromState, propsFromDispatch, ownProps) => {
  const {
    showingSearchResults,
    searchQuery: { restaurantName, foodName },
  } = propsFromState
  return {
    ...ownProps,
    ...propsFromState,
    ...propsFromDispatch,
    onShowMoreClick: () => {
      if (showingSearchResults) searchRestaurants(restaurantName, foodName)
      else withLoading(fetchRestaurants)()
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(Home)
