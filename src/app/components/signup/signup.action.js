// modules
import { createAction } from 'redux-actions'
// setup
import { dispatch } from '../../../setup/redux'

export const SET_SINGUP_DATA = 'SET_SINGUP_DATA'
export const dispatchSetSignupData = (...args) =>
  dispatch(createAction(SET_SINGUP_DATA)(...args))
