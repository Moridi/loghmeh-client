// modules
import { connect } from 'react-redux'
// components
import Signup from './signup'
// actions
import { dispatchSetSignupData } from './signup.action'
// requests
import { signupUser } from '../../../logic/user/user.request'
import { validateCredentials } from './signup.helper'

const mapStateToProps = state => ({
  values: state.view.signup,
})

const mapDispatchToProps = () => ({
  onChange: key => value => dispatchSetSignupData({ [key]: value }),
  onSignup: values => {
    if (!validateCredentials(values)) return

    const { firstName, lastName, phone, email, password } = values
    signupUser({ firstName, lastName, phone, username: email, password })
  },
})

export default connect(mapStateToProps, mapDispatchToProps)(Signup)
