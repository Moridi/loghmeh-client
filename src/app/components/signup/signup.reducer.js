const initialState = {
  firstName: '',
  lastName: '',
  phone: '',
  email: '',
  password: '',
  verifyPassword: '',
}

const reducers = {
  SET_SINGUP_DATA: (state, data) => ({
    ...state,
    ...data,
  }),
}

export default (state = initialState, { type, payload }) =>
  reducers[type] ? reducers[type](state, payload) : state
