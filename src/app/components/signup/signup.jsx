import React from 'react'
import PropTypes from 'prop-types'
// components
import Input from '../../../helper/components/input/input'
import Footer from '../../../helper/components/footer/footer'
// style
import './signup.css'

const Signup = ({ values, onChange, onSignup }) => {
  return (
    <div className='signup-container'>
      <div class='signup-body'>
        <img
          className='home-cover signup-background'
          src='/images/Cover.jpg'
          alt='background'
        ></img>
        <img class='signup-logo' src='/images/LOGO.png' alt='لقمه' />
        <div class='signup-panel'>
          <div className='signup-name-title'>
            <p className='signup-title short'>نام خانوادگی</p>
            <p className='signup-title short'>نام</p>
          </div>
          <div className='signup-name-inputs'>
            <Input
              value={values.firstName}
              placeholder='نام'
              className='signup-input short'
              onChange={onChange('firstName')}
            />
            <Input
              value={values.lastName}
              placeholder='نام خانوادگی'
              className='signup-input short'
              onChange={onChange('lastName')}
            />
          </div>
          <p className='signup-title'>شماره همراه</p>
          <Input
            value={values.phone}
            placeholder='مثال: ۰۹۱۲۳۴۵۶۷۸۹'
            className='signup-input'
            onChange={onChange('phone')}
          />
          <p className='signup-title'>ایمیل</p>
          <Input
            value={values.email}
            placeholder='مثال: example@loghmeh.ir'
            className='signup-input'
            onChange={onChange('email')}
          />
          <span class='pass-divider'></span>
          <p className='signup-title'>رمز عبور</p>
          <Input
            value={values.password}
            placeholder='رمز عبور'
            className='signup-input'
            type='password'
            onChange={onChange('password')}
          />
          <Input
            value={values.verifyPassword}
            placeholder='تکرار رمز عبور'
            className='signup-input'
            type='password'
            onChange={onChange('verifyPassword')}
          />
        </div>
        <button class='btn signup-button' onClick={() => onSignup(values)}>
          ثبت نام
        </button>
      </div>
      <Footer />
    </div>
  )
}

Signup.propTypes = {
  values: PropTypes.shape({}).isRequired,
  onChange: PropTypes.func.isRequired,
  onSignup: PropTypes.func.isRequired,
}

export default Signup
