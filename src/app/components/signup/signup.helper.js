import { dispatchSetSnackbar } from '../snackbar/snackbar.actions'

const validatePhone = phone => {
  const matches = phone.match(/[+]?([0-9]+|[۰-۹]+)/g)
  if (!matches || matches.length !== 1) return false
  return matches[0].length === phone.length
}

export const validateCredentials = ({
  firstName,
  lastName,
  phone,
  email,
  password,
  verifyPassword,
}) => {
  if (
    !firstName ||
    !lastName ||
    !phone ||
    !email ||
    !password ||
    !verifyPassword
  ) {
    dispatchSetSnackbar('لطفا تمام مقادیر را وارد کنید', 'error')
    return false
  }
  if (password !== verifyPassword) {
    dispatchSetSnackbar('تکرار رمز عبور صحیح نمی‌باشد', 'error')
    return false
  }
  if (!validatePhone(phone)) {
    dispatchSetSnackbar('شماره همراه وارد شده صحیح نمی‌باشد', 'error')
    return false
  }
  return true
}
