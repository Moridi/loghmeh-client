// modules
import React from 'react'
import { LocationProvider, Router } from '@reach/router'
import { Provider } from 'react-redux'
// components
import Navigator from './components/navigator/navigator.container'
import Signup from './components/signup/signup.container'
import Login from './components/login/login.container'
import Profile from './components/profile/profile.container'
import UserCart from './components/userCart/userCart.container'
import Home from './components/home/home.container'
import Restaurant from './components/restaurant/restaurant.container'
import LoadingPage from './components/loadingPage/loadingPage'
import FoodDetails from './components/foodDetails/foodDetails.container'
import SnackBar from './components/snackbar/snackbar.container'
import Loading from './components/loading/loading.container'
// styles
import './app.css'
// setup
import store from '../setup/redux'
import { history } from '../setup/history.js'

const App = () => (
  <Provider store={store}>
    <LocationProvider history={history}>
      <Router>
        <Navigator path=':page' />
        <Navigator path=':page/:id' />
      </Router>
      <UserCart />
      <FoodDetails />
      <SnackBar />
      <Loading />
      <div className='app'>
        <Router className='full'>
          <Signup path='signup' />
          <Login path='login' />
          <Profile path='profile' />
          <Home path='home' />
          <Restaurant path='restaurant/:id' />
          <LoadingPage path='loading' />
        </Router>
      </div>
    </LocationProvider>
  </Provider>
)

export default App
