// modules
import { combineReducers } from 'redux'
// reducers
import signup from './components/signup/signup.reducer'
import userCart from './components/userCart/userCart.reducer'
import foodDetails from './components/foodDetails/foodDetails.reducer'
import restaurant from './components/restaurant/restaurant.reducer'
import snackbar from './components/snackbar/snackbar.reducer'
import loading from './components/loading/loading.reducer'
import home from './components/home/home.reducer'

export default combineReducers({
  home,
  signup,
  userCart,
  foodDetails,
  restaurant,
  snackbar,
  loading,
})
